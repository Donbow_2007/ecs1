/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ecs;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author BowdenDonnell
 */
public class Inventory {
    private String name;
    private String description; 
    private String location;
    private int stock;
    private ArrayList<Tools> tools;
    private int manager = 0;

    public Inventory() {
        this.name = "NA";
        this.description = "NA";
        this.stock = 0;
        /* Creates a empty Tool*/
        //Tools tool = new Tools();
        //this.tools.add(tool);
    }

    public Inventory(String name, String discription, int stock, ArrayList<Tools> tools) {
        this.name = name;
        this.description = discription;
        this.stock = stock;
        this.tools = tools;
    }
    
    public ArrayList<Inventory> getInventoryList(){
        ArrayList<Inventory> iList = new ArrayList();
        
        DataIO data = new DataIO();
        try {
            iList = data.getInvantoryList();
            /* get invantory List from Data IO class */
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Inventory.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(Inventory.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return iList;
    }
    
    public boolean checkToolState(String toolID){
        boolean state = false;
        DataIO data = new DataIO();
        if(data.checkToolState(toolID)){
            state = true;
        }
        return state;
    }
    
    public boolean changeToolState(String inventoryID, String employeeID, String state){
        /*This will Change the state of the tool that has been selected. */
        boolean tf = false;
        DataIO data = new DataIO();
        if(data.setToolState(inventoryID,employeeID,state)){ 
            tf = true;
        }
        return tf;
    }
    
    public void updateStock(){
        /* This sends the new list of data from this class to the data IO class */
    }
    
    public void managerMode(){
        manager = 1;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public ArrayList<Tools> getTools() {
        return tools;
    }

    public void setTools(ArrayList<Tools> tools) {
        this.tools = tools;
    }
    
    
    
}

