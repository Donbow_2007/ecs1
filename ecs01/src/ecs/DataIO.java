/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ecs;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author BowdenDonnell
 */
public class DataIO {

    /* Database Conection Class */
    private final String DATABASE_NAME = "csis420";
    private final String CONNECTION_STRING = "jdbc:mysql://localhost:3306/" + DATABASE_NAME;
    private final String USER_NAME = "root";
    private final String PASSWORD = "";

    public DataIO() {
        /* Empty */
    }

    public ArrayList<Inventory> getInvantoryList() throws ClassNotFoundException, SQLException {
        ArrayList<Inventory> list = new ArrayList<Inventory>();

        //connect to database 
        Connection conn = DriverManager.getConnection(CONNECTION_STRING,
                USER_NAME, PASSWORD);

        Statement statement = conn.createStatement();
        String SQL = "SELECT * , count(name) as stock FROM `csis420`.`tools` GROUP BY `name`;";
        ResultSet rs = statement.executeQuery(SQL);

        while (rs.next()) {
            // create Customer object and load the attributes
            System.out.println(rs.getString(2));

            Inventory inv = new Inventory();

            inv.setName(rs.getString(2));
            inv.setDescription(rs.getString(3));
            inv.setStock(rs.getInt(7));
            // add the Customer object to our list
            list.add(inv);
        }
        rs.close();
        for(int I = 0; I < list.size(); I++){
            ArrayList<Tools> tools = new ArrayList<Tools>();
            String deviceName = list.get(I).getName();
            String SQL2 = "SELECT * FROM `csis420`.`tools` WHERE `name` LIKE '" + deviceName + "';";
            ResultSet rs2 = statement.executeQuery(SQL2);
            while (rs2.next()) {
                Tools t = new Tools();
                t.setTool_ID(rs2.getInt(1));
                t.setName(rs2.getString(2));
                t.setDiscription(rs2.getString(3));
                t.setLocation(rs2.getString(4));
                t.setToolState(rs2.getString(5));
                tools.add(t);
            }
            list.get(I).setTools(tools);
            rs2.close();
            
        }
        
        
        
        // close the database connection
        conn.close();

        // return the ArrayList
        return list;
    }

    public boolean validateEmployeeID(String ID) {
        boolean tf = false;
        Connection conn;
        try {
            conn = DriverManager.getConnection(CONNECTION_STRING,
                    USER_NAME, PASSWORD);
            Statement statement = conn.createStatement();
            String SQL = "SELECT * FROM `csis420`.`Employee` WHERE empID = '" + ID + "';";
            ResultSet rs = statement.executeQuery(SQL);
            while (rs.next()) {
                tf = true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DataIO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return tf;
    }

    public boolean checkToolState(String id) {
        boolean tf = false;
        Connection conn;
        try {
            conn = DriverManager.getConnection(CONNECTION_STRING,
                    USER_NAME, PASSWORD);
            Statement statement = conn.createStatement();
            String SQL = "SELECT * FROM `csis420`.`tools` WHERE toolID = '" + id + "' AND `state` = 'available';";
            ResultSet rs = statement.executeQuery(SQL);
            while (rs.next()) {
                tf = true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DataIO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return tf;
    }

    public boolean setToolState(String inventoryID, String employeeID, String state) {
        boolean tf = false;
        Connection conn;
        try {
            conn = DriverManager.getConnection(CONNECTION_STRING,
                    USER_NAME, PASSWORD);

            PreparedStatement st = conn.prepareStatement("UPDATE `csis420`.`tools` SET `state` = ? WHERE toolID = ?");
            st.setString(1, state);
            st.setString(2, inventoryID);
            st.executeUpdate();
            tf = true;
        } catch (SQLException ex) {
            Logger.getLogger(DataIO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return tf;
    }

}
