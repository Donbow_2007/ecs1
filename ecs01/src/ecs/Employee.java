/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ecs;

import java.sql.SQLException;

/**
 *
 * @author BowdenDonnell
 */
public class Employee {
    private int empID;
    private String name;
    private String DOB;
    private String address;
    private String email;
    private String job;
    private int level;
    private String equipmentList;

    public Employee(){
        this.empID = 0;
        this.name = "NA";
        this.DOB = "00/00/0000";
        this.address = "NA";
        this.email = "NA";
        this.job = "NA";
        this.level = 0;
        this.equipmentList = "NA";
    }

    public Employee(int empID, String name, String DOB, String address, String email, String job, int level, String equipmentList) {
        this.empID = empID;
        this.name = name;
        this.DOB = DOB;
        this.address = address;
        this.email = email;
        this.job = job;
        this.level = level;
        this.equipmentList = equipmentList;
    }
    
    public boolean validatesEmployment(String id_Number) throws SQLException{
        boolean tf = false;
        
        DataIO data = new DataIO();
        tf = data.validateEmployeeID(id_Number);
        return tf;
    }

    public int getEmpID() {
        return empID;
    }

    public void setEmpID(int empID) {
        this.empID = empID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getEquipmentList() {
        return equipmentList;
    }

    public void setEquipmentList(String equipmentList) {
        this.equipmentList = equipmentList;
    }
            
}
