/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ecs;

/**
 *
 * @author BowdenDonnell
 */
public class Tools {
    
    private int tool_ID;
    private String name;
    private String discription;
    private Location location;
    private ToolState toolState;
    
    /* Enumerations */
    enum Location { warehouseA, warehouseB}
    enum ToolState{ available, checkedOut, lost, stolen, damaged}

    public Tools() {
        this.tool_ID = 0;
        this.name = "NA";
        this.discription = "Empty";
        this.location = Location.warehouseA;
        this.toolState = ToolState.available;
    }

    public Tools(int tool_ID, String name, String discription, Location location, ToolState toolState) {
        this.tool_ID = tool_ID;
        this.name = name;
        this.discription = discription;
        this.location = location;
        this.toolState = toolState;
    } 
    
    public int getTool_ID() {
        return tool_ID;
    }

    public void setTool_ID(int tool_ID) {
        this.tool_ID = tool_ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDiscription() {
        return discription;
    }

    public void setDiscription(String discription) {
        this.discription = discription;
    }

    public String getLocation() {
        return location.toString();
    }

    public void setLocation(String location) {
        this.location = Location.valueOf(location);
    }

    public String getToolState() {
        return toolState.toString();
    }

    public void setToolState(String toolState) {
        this.toolState =  ToolState.valueOf(toolState);
    }

    
    
    
    
    
    
}
